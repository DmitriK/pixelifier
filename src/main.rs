use std::env;
use std::fs::File;
use std::io::Read;
use std::process::Command;

mod parser;

use parser::{BdfChar, BdfData, Bitmap, BoundingBox, Property};

const STRENGTH: u32 = 2;

fn main() {
    let src_font = env::args()
        .nth(1)
        .expect("Must pass in path to source font file as first argument");

    let dst_name = env::args()
        .nth(2)
        .expect("Must pass in name of destination font file as second argument");

    let mut f = File::open(src_font).expect("Failed to open source font file");
    let mut buffer = String::new();
    f.read_to_string(&mut buffer).unwrap();
    // let f = BufReader::new(f);

    let bdf_data = parser::parse_bdf(buffer.lines()).expect("Failed to parse BDF");
    let mut bdf_new = BdfData {
        bounding_box: BoundingBox {
            width: (STRENGTH + 1) * bdf_data.bounding_box.width - 1,
            height: (STRENGTH + 1) * bdf_data.bounding_box.height - 1,
            lower_left_x: (STRENGTH as i32 + 1) * bdf_data.bounding_box.lower_left_x,
            lower_left_y: (STRENGTH as i32 + 1) * bdf_data.bounding_box.lower_left_y,
        },
        chars: vec![],
        dpi_x: bdf_data.dpi_x,
        dpi_y: bdf_data.dpi_y,
        face: bdf_data.face,
        num_chars: bdf_data.num_chars,
        size: (STRENGTH + 1) * bdf_data.size - 1,
        version: bdf_data.version,
        properties: bdf_data.properties,
    };

    let new_point_width = 10 * bdf_new.bounding_box.width;

    if let Some(old_size) = bdf_new.face.pixel_size {
        let new_size = (STRENGTH + 1) * old_size - 1;
        bdf_new.face.pixel_size = Some(new_size);
        bdf_new.face.point_size = Some(10 * new_size);
    }

    bdf_new.face.average_width = Some(new_point_width);
    bdf_new.face.family_name += &format!("Pixel{}x", STRENGTH);

    bdf_new
        .properties
        .entry("PIXEL_SIZE".into())
        .and_modify(|val| {
            if let Property::Num(old) = val {
                *val = Property::Num((STRENGTH as i32 + 1) * *old - 1);
            }
        });
    bdf_new
        .properties
        .entry("POINT_SIZE".into())
        .and_modify(|val| {
            if let Property::Num(old) = val {
                *val = Property::Num((STRENGTH as i32 + 1) * *old);
            }
        });
    bdf_new
        .properties
        .entry("FONT_ASCENT".into())
        .and_modify(|val| {
            if let Property::Num(old) = val {
                *val = Property::Num((STRENGTH as i32 + 1) * *old);
            }
        });
    bdf_new
        .properties
        .entry("FONT_DESCENT".into())
        .and_modify(|val| {
            if let Property::Num(old) = val {
                *val = Property::Num((STRENGTH as i32 + 1) * *old);
            }
        });
    bdf_new
        .properties
        .entry("CAP_HEIGHT".into())
        .and_modify(|val| {
            if let Property::Num(old) = val {
                *val = Property::Num((STRENGTH as i32 + 1) * *old);
            }
        });
    bdf_new
        .properties
        .entry("AVERAGE_WIDTH".into())
        .and_modify(|val| {
            if let Property::Num(_) = val {
                *val = Property::Num(new_point_width as i32);
            }
        });
    bdf_new
        .properties
        .entry("MIN_SPACE".into())
        .and_modify(|val| {
            if let Property::Num(_) = val {
                *val = Property::Num(new_point_width as i32);
            }
        });
    bdf_new
        .properties
        .entry("FAMILY_NAME".into())
        .and_modify(|val| {
            if let Property::Str(v) = val {
                *v += &format!("Pixel{}x", STRENGTH);
            }
        });

    println!();

    for (index, chr) in bdf_data.chars.iter().enumerate() {
        print!(
            "\rProgress: {}/{} {}% [{:04x}: {}]",
            index,
            bdf_new.num_chars,
            100 * index / bdf_new.num_chars,
            chr.encoding,
            chr.code
        );

        let bbox = chr.bounding_box.as_ref().unwrap_or(&bdf_data.bounding_box);
        let new_bbox = BoundingBox {
            width: (STRENGTH + 1) * bbox.width - 1,
            height: (STRENGTH + 1) * bbox.height - 1,
            lower_left_x: (STRENGTH as i32 + 1) * bbox.lower_left_x,
            lower_left_y: (STRENGTH as i32 + 1) * bbox.lower_left_y,
        };

        let mut new_bmap = Bitmap::new(&new_bbox);

        // println!("┌{}┐", "─".repeat(bbox.width as usize));
        let mut row_iter = (0..bbox.height).peekable();
        while let Some(row) = row_iter.next() {
            // print!("│");
            let mut bit_index = 7;
            let mut working_byte = 0;

            let mut new_row_vec = Vec::with_capacity(new_bbox.width as usize);

            let mut col_iter = (0..bbox.width).peekable();
            while let Some(col) = col_iter.next() {
                let mut push_bit = |b: bool| {
                    if b {
                        working_byte |= 1 << bit_index;
                    }
                    bit_index -= 1;
                    if bit_index < 0 {
                        new_row_vec.push(working_byte);
                        bit_index = 7;
                        working_byte = 0;
                    }
                };

                if chr.bitmap.index(row, col) {
                    // print!("X");
                    for _ in 0..STRENGTH {
                        push_bit(true);
                    }
                } else {
                    // print!(" ");
                    for _ in 0..STRENGTH {
                        push_bit(false);
                    }
                }

                if col_iter.peek().is_some() {
                    push_bit(false);
                } else {
                    if bit_index != 7 {
                        new_row_vec.push(working_byte);
                    }
                }

                // println!("{:?}, {:?}", row, col);
            }
            // print!("│");
            // print!("\n");

            for _ in 0..STRENGTH {
                new_bmap.data.push(new_row_vec.clone());
            }
            if row_iter.peek().is_some() {
                let blank_vec = vec![0; new_bmap.stride as usize];
                new_bmap.data.push(blank_vec);
            }
        }
        // println!("└{}┘", "─".repeat(bbox.width as usize));

        // println!("┌{}┐", "─".repeat(new_bbox.width as usize));
        // for row in 0..new_bbox.height {
        //     print!("│");

        //     for col in 0..new_bbox.width {
        //         if new_bmap.index(row, col) {
        //             print!("X");
        //         } else {
        //             print!(" ");
        //         }
        //     }
        //     print!("│");
        //     print!("\n");
        // }
        // println!("└{}┘", "─".repeat(new_bbox.width as usize));

        let new_char = BdfChar {
            encoding: chr.encoding,
            bitmap: new_bmap,
            bounding_box: Some(new_bbox),
            code: chr.code.clone(),
            scaleable_width_x: chr.scaleable_width_x,
            scaleable_width_y: chr.scaleable_width_y,
            device_width_x: (STRENGTH + 1) * chr.device_width_x - 1,
            device_width_y: if chr.device_width_y > 0 {
                (STRENGTH + 1) * chr.device_width_y - 1
            } else {
                0
            },
        };

        bdf_new.chars.push(new_char);
    }

    println!();

    let bdf_name = format!("{}.bdf", dst_name);

    let mut fo = File::create(&bdf_name).expect("Failed to create destination font file");
    bdf_new
        .write_out(&mut fo)
        .expect("Failed to write output file");

    let ods_name = format!("{}.ods", dst_name);

    Command::new("fonttosfnt")
        .arg("-o")
        .arg(ods_name)
        .arg(bdf_name)
        .spawn()
        .expect("Failed to convert bdf to ods");

    // println!("{:?}", bdf_data);
}
