use simple_error::SimpleError;
use std::convert::TryInto;
use std::fmt;
use std::io::Write;
use std::str::FromStr;
use std::str::Lines;

use std::collections::HashMap;

#[derive(Default, Debug)]
pub struct BoundingBox {
    pub width: u32,
    pub height: u32,
    pub lower_left_x: i32,
    pub lower_left_y: i32,
}

#[derive(Default, Debug)]
pub struct Bitmap {
    width: u32,
    height: u32,
    pub stride: u32,
    pub data: Vec<Vec<u8>>,
}

impl Bitmap {
    pub fn new(bbox: &BoundingBox) -> Self {
        let stride = (bbox.width as f32 / 8.0).ceil() as u32;
        // let vec_size = (stride * bbox.height) as usize;
        Self {
            width: bbox.width,
            height: bbox.height,
            stride,
            data: Vec::with_capacity(bbox.height as usize),
        }
    }
    fn validate_size(&self) -> Result<(), SimpleError> {
        if self.height as usize != self.data.len() {
            return Err(SimpleError::new(format!(
                "Failed bitmap size validation. {}x{} bitmap has {} rows",
                self.width,
                self.height,
                self.data.len()
            )));
        }
        let vec_size = (self.width as f32 / 8.0).ceil() as usize;
        for (index, row) in self.data.iter().enumerate() {
            if vec_size != row.len() {
                return Err(SimpleError::new(format!(
                    "Failed bitmap size validation. {}x{} bitmap has {} columns on row {}",
                    self.width,
                    self.height,
                    row.len(),
                    index
                )));
            }
        }
        Ok(())
    }
    pub fn index(&self, row: u32, col: u32) -> bool {
        let byte_offset = (col / 8) as usize;
        let bit_offset = 7 - col % 8;
        ((0x1 & (self.data[row as usize][byte_offset] >> bit_offset)) == 1)
    }
}

#[derive(Default, Debug)]
pub struct BdfChar {
    pub code: String,
    pub encoding: u32,
    pub scaleable_width_x: u32,
    pub scaleable_width_y: u32,
    pub device_width_x: u32,
    pub device_width_y: u32,
    // meta_data: HashMap<String, String>,
    pub bounding_box: Option<BoundingBox>,
    pub bitmap: Bitmap,
}

#[derive(Debug)]
pub enum Property {
    Num(i32),
    Str(String),
}

#[derive(Debug, Default)]
pub struct XLogicalFontDesc {
    foundry: String,                // Type foundry - vendor or supplier of this font
    pub family_name: String,        // Typeface family
    weight_name: String,            // Weight of type
    slant: String, // Slant (upright, italic, oblique, reverse italic, reverse oblique, or "other")
    setwidth_name: String, // Proportionate width (e.g. normal, condensed, narrow, expanded/double-wide)
    add_style_name: String, // Additional style (e.g. (Sans) Serif, Informal, Decorated)
    pub pixel_size: Option<u32>, // Size of characters, in pixels; 0 (Zero) means a scalable font
    pub point_size: Option<u32>, // Size of characters, in tenths of points
    resolution_x: Option<u32>, // Horizontal resolution in dots per inch (DPI), for which the font was designed
    resolution_y: Option<u32>, // Vertical resolution, in DPI
    spacing: String,           // monospaced, proportional, or "character cell"
    pub average_width: Option<u32>, // Average width of characters of this font; 0 means scalable font
    charset_registry: String,       // Registry defining this character set
    charset_encoding: String,       // Registry's character encoding scheme for this set
}

impl XLogicalFontDesc {
    fn new(desc: &str) -> Self {
        let mut values = desc.split("-");
        // Skip initial hyphen
        values.next();
        Self {
            foundry: values.next().unwrap_or("").into(),
            family_name: values.next().unwrap_or("").into(),
            weight_name: values.next().unwrap_or("").into(),
            slant: values.next().unwrap_or("").into(),
            setwidth_name: values.next().unwrap_or("").into(),
            add_style_name: values.next().unwrap_or("").into(),
            pixel_size: values.next().and_then(|x| x.parse().ok()),
            point_size: values.next().and_then(|x| x.parse().ok()),
            resolution_x: values.next().and_then(|x| x.parse().ok()),
            resolution_y: values.next().and_then(|x| x.parse().ok()),
            spacing: values.next().unwrap_or("").into(),
            average_width: values.next().and_then(|x| x.parse().ok()),
            charset_registry: values.next().unwrap_or("").into(),
            charset_encoding: values.next().unwrap_or("").into(),
        }
    }
}

impl fmt::Display for XLogicalFontDesc {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let foo = [
            self.foundry.as_ref(),
            self.family_name.as_ref(),
            self.weight_name.as_ref(),
            self.slant.as_ref(),
            self.setwidth_name.as_ref(),
            self.add_style_name.as_ref(),
            self.pixel_size
                .map(|p| format!("{}", p))
                .unwrap_or("".into())
                .as_ref(),
            self.point_size
                .map(|p| format!("{}", p))
                .unwrap_or("".into())
                .as_ref(),
            self.resolution_x
                .map(|p| format!("{}", p))
                .unwrap_or("".into())
                .as_ref(),
            self.resolution_y
                .map(|p| format!("{}", p))
                .unwrap_or("".into())
                .as_ref(),
            self.spacing.as_ref(),
            self.average_width
                .map(|p| format!("{}", p))
                .unwrap_or("".into())
                .as_ref(),
            self.charset_registry.as_ref(),
            self.charset_encoding.as_ref(),
        ]
        .join("-");
        write!(f, "-{}", foo)
    }
}

#[derive(Debug)]
pub struct BdfData {
    pub version: f32,
    pub face: XLogicalFontDesc,
    pub size: u32,
    pub dpi_x: u32,
    pub dpi_y: u32,
    pub bounding_box: BoundingBox,
    pub num_chars: usize,
    // meta_data: HashMap<String, String>,
    pub properties: HashMap<String, Property>,
    pub chars: Vec<BdfChar>,
}

impl BdfData {
    pub fn write_out(&self, output: &mut dyn Write) -> std::io::Result<()> {
        writeln!(output, "STARTFONT {}", self.version)?;
        writeln!(output, "FONT {}", self.face)?;
        writeln!(output, "SIZE {} {} {}", self.size, self.dpi_x, self.dpi_y)?;
        writeln!(
            output,
            "FONTBOUNDINGBOX {} {} {} {}",
            self.bounding_box.width,
            self.bounding_box.height,
            self.bounding_box.lower_left_x,
            self.bounding_box.lower_left_y
        )?;
        writeln!(output, "STARTPROPERTIES {}", self.properties.len())?;
        for property in &self.properties {
            match property {
                (key, Property::Num(n)) => writeln!(output, "{} {}", key, n)?,
                (key, Property::Str(s)) => writeln!(output, "{} \"{}\"", key, s)?,
            }
        }
        writeln!(output, "ENDPROPERTIES")?;
        writeln!(output, "CHARS {}", self.chars.len())?;
        for chr in &self.chars {
            writeln!(output, "STARTCHAR {}", chr.code)?;
            writeln!(output, "ENCODING {}", chr.encoding)?;
            writeln!(
                output,
                "SWIDTH {} {}",
                chr.scaleable_width_x, chr.scaleable_width_y
            )?;
            writeln!(
                output,
                "DWIDTH {} {}",
                chr.device_width_x, chr.device_width_y
            )?;
            if let Some(bbx) = &chr.bounding_box {
                writeln!(
                    output,
                    "BBX {} {} {} {}",
                    bbx.width, bbx.height, bbx.lower_left_x, bbx.lower_left_y
                )?;
            }
            writeln!(output, "BITMAP")?;
            for row in 0..chr
                .bounding_box
                .as_ref()
                .unwrap_or(&self.bounding_box)
                .height
            {
                let row_data = &chr.bitmap.data[row as usize];
                for byte in row_data {
                    write!(output, "{:02x}", byte)?;
                }
                writeln!(output)?;
            }
            writeln!(output, "ENDCHAR")?;
        }
        writeln!(output, "ENDFONT")?;

        Ok(())
    }
}

fn get_key_val(line: &str) -> (&str, &str) {
    let index = line.find(" ").unwrap_or_else(|| line.len());
    let (key, val) = line.split_at(index);
    let val = val.trim();
    (key, val)
}

fn get_num_array<T>(val: &str) -> Vec<T>
where
    T: FromStr,
{
    val.split_whitespace()
        .filter_map(|s| s.parse::<T>().ok())
        .collect()
}

fn parse_bitmap(lines: &mut Lines, bbox: &BoundingBox) -> Result<Bitmap, SimpleError> {
    let mut res = Bitmap::new(bbox);
    loop {
        match lines.next() {
            Some("ENDCHAR") => break,
            Some(x) => {
                let mut remaining = x;
                let mut row_vec = Vec::with_capacity(bbox.width as usize);
                while remaining.len() >= 2 {
                    let (byte_string, new_remaining) = remaining.split_at(2);
                    remaining = new_remaining;
                    if let Ok(val) = u8::from_str_radix(byte_string, 16) {
                        row_vec.push(val);
                    } else {
                        panic!("Couldn't parse bitmap line: {:?}", x);
                    }
                }
                res.data.push(row_vec);
            }
            None => break,
        }
    }
    res.validate_size()?;
    Ok(res)
}

fn parse_char(code: &str, lines: &mut Lines, bbox: &BoundingBox) -> Result<BdfChar, SimpleError> {
    let mut res = BdfChar::default();
    res.code = code.into();
    loop {
        match lines.next() {
            Some(line) => {
                let (key, val) = get_key_val(line);
                match key {
                    "ENDCHAR" => {
                        return Err(SimpleError::new("Reached ENDCHAR without BITMAP"));
                    }
                    "ENCODING" => {
                        res.encoding = val
                            .parse::<u32>()
                            .expect(&format!("Failed to read character encoding: {}", line));
                    }
                    "SWIDTH" => {
                        let vals = get_num_array::<u32>(val);
                        if vals.len() != 2 {
                            panic!("SWIDTH property didn't have enough data: {}", line);
                        }
                        res.scaleable_width_x = vals[0];
                        res.scaleable_width_y = vals[1];
                    }
                    "DWIDTH" => {
                        let vals = get_num_array::<u32>(val);
                        if vals.len() != 2 {
                            panic!("DWIDTH property didn't have enough data: {}", line);
                        }
                        res.device_width_x = vals[0];
                        res.device_width_y = vals[1];
                    }
                    "BBX" => {
                        let vals = get_num_array::<i32>(val);
                        if vals.len() != 4 {
                            return Err(SimpleError::new(format!(
                                "BBX property didn't have enough data: {}",
                                line,
                            )));
                        }
                        let bbx = BoundingBox {
                            width: vals[0]
                                .try_into()
                                .expect(&format!("Got unexpected negative width: {}", line)),
                            height: vals[1]
                                .try_into()
                                .expect(&format!("Got unexpected negative width: {}", line)),
                            lower_left_x: vals[2],
                            lower_left_y: vals[3],
                        };
                        res.bounding_box = Some(bbx);
                    }
                    "BITMAP" => {
                        res.bitmap =
                            parse_bitmap(lines, res.bounding_box.as_ref().unwrap_or(&bbox))?;
                        return Ok(res);
                    }
                    _ => {
                        panic!("Got unexpected metadata in CHAR: {}", line);
                        // res.meta_data.insert(key.into(), val.into());
                    }
                };
            }
            None => break,
        }
    }
    Err(SimpleError::new("Reached EOF without ENDCHAR"))
}

fn parse_props(lines: &mut Lines) -> HashMap<String, Property> {
    let mut res = HashMap::new();
    loop {
        match lines.next() {
            Some(line) => {
                let (key, val) = get_key_val(line);
                if key == "ENDPROPERTIES" {
                    return res;
                }

                if val.starts_with("\"") && val.ends_with("\"") {
                    // If quoted string, extract string
                    let de_quoted = val.trim_start_matches("\"").trim_end_matches("\"");
                    res.insert(key.into(), Property::Str(de_quoted.into()));
                } else if let Ok(num) = val.parse::<i32>() {
                    // Otherwise try to parse a number
                    res.insert(key.into(), Property::Num(num));
                } else {
                    eprintln!("Got unexpected property {}: {}", key, val);
                    break;
                }
            }
            None => break,
        }
    }
    eprintln!("Reached end of BDF without ENDPROPERTIES");
    res
}

pub fn parse_bdf(mut lines: Lines) -> Option<BdfData> {
    let line = lines.next()?;
    let (first_key, first_val) = get_key_val(line);

    if first_key != "STARTFONT" {
        eprintln!("BDF does not start with STARTFONT");
        return None;
    }
    // Check for some kind of version number
    let version = first_val.parse::<f32>().ok()?;

    let mut face = None;
    let mut size = None;
    let mut dpi_x = None;
    let mut dpi_y = None;
    let mut bounding_box = None;
    let mut num_chars = None;
    let mut chars = vec![];
    let mut properties = None;

    loop {
        let line = match lines.next() {
            Some(l) => l,
            None => {
                eprintln!("Got EOF before ENDFONT");
                break;
            }
        };
        let (key, val) = get_key_val(line);
        match key {
            "ENDFONT" => break,
            "FONT" => face = Some(XLogicalFontDesc::new(val)),
            "SIZE" => {
                let vals = get_num_array::<u32>(val);
                if vals.len() != 3 {
                    eprintln!("SIZE property didn't have enough data: {:?}", val);
                    return None;
                }
                size = Some(vals[0]);
                dpi_x = Some(vals[1]);
                dpi_y = Some(vals[2]);
            }
            "FONTBOUNDINGBOX" => {
                let vals = get_num_array::<i32>(val);
                if vals.len() != 4 {
                    eprintln!(
                        "FONTBOUNDINGBOX property didn't have enough data: {:?}",
                        val
                    );
                    return None;
                }
                bounding_box = Some(BoundingBox {
                    width: vals[0].try_into().expect(&format!(
                        "Got unexpected negative bounding box width: {}",
                        val
                    )),
                    height: vals[1].try_into().expect(&format!(
                        "Got unexpected negative bounding box height: {}",
                        val
                    )),
                    lower_left_x: vals[2],
                    lower_left_y: vals[3],
                });
            }
            "CHARS" => {
                num_chars = Some(
                    val.parse::<usize>()
                        .expect(&format!("Failed to read number of characters: {}", val)),
                );
                chars = Vec::with_capacity(num_chars?);
            }
            "STARTPROPERTIES" => properties = Some(parse_props(&mut lines)),
            "STARTCHAR" => {
                if val != "" {
                    match parse_char(val.into(), &mut lines, bounding_box.as_ref()?) {
                        Ok(chr) => {
                            chars.push(chr);
                        }
                        Err(e) => eprintln!("Failed adding character {}: {}", val, e),
                    }
                } else {
                    panic!("Found char with no char name");
                }
            }
            _ => {
                panic!("Got unexpected metadata: {}", line);
                // if val != "" {
                //     res.meta_data.insert(key.into(), val.into());
                // } else {
                //     eprintln!("Got unknown bare key {:?}", key);
                // }
            }
        }
    }
    if num_chars != Some(chars.len()) {
        panic!("Incorrect number of characters!");
    }
    Some(BdfData {
        properties: properties?,
        bounding_box: bounding_box?,
        chars: chars,
        num_chars: num_chars?,
        dpi_x: dpi_x?,
        dpi_y: dpi_y?,
        face: face?,
        size: size?,
        version: version,
    })
}
